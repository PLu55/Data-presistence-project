using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public string playerName;
    public string highScorePlayerName;
    public int highScore;
    public string highScoreText;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        LoadHighScore();
        Debug.Log("GameManager.Awake");
    }

    void OnApplicationQuit()
    {
        SaveHighScore();
    }
    [System.Serializable]
    class HighScoreData
    {
        public string playerName;
        public int highScore;
    }
    public string GetHighScoreText()
    {
        return string.Format("High Score : {0} : {1}", highScorePlayerName, highScore);
    }

    public void SaveHighScore()
    {
        HighScoreData data = new HighScoreData();
        data.playerName = highScorePlayerName;
        data.highScore = highScore;

        string json = JsonUtility.ToJson(data);
    
        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }
    public void LoadHighScore()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            HighScoreData data = JsonUtility.FromJson<HighScoreData>(json);

            highScorePlayerName = data.playerName;
            highScore = data.highScore;
        }
        else
        {
            highScorePlayerName = "Nobody";
            highScore = 0;
        }
    }
    public void CheckHighScore(int score)
    {
        if (score > highScore)
        {
            highScore = score;
            highScorePlayerName = playerName;
        }
    }
}
