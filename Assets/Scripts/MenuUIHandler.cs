using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif

[DefaultExecutionOrder(1000)]
public class MenuUIHandler : MonoBehaviour
{
    public GameObject playerNameField;
    public GameObject highScoreText;

    void Start()
    {
        Debug.Log("MenuUIHandler.Start");
        Load();
    }
    public void StartNew()
    {
        GameManager.Instance.playerName = GetPlayerName ();
        SceneManager.LoadScene(1);
    }
    
    public void Exit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }

    public void Load()
    {
        highScoreText.GetComponent<TMP_Text> ().text = GameManager.Instance.GetHighScoreText();
    }

    public string GetPlayerName ()
    {
        string text = playerNameField.GetComponent<TMP_InputField>().text;
        return text;
    }
}
